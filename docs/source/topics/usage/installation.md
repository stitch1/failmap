# Installation

## Development

If you just want to run failmap for development, read [development/getting_started.html](../development/getting_started.html).

## Self-hosted

If you want to host failmap with all of it's dependencies on a dedicated server, visit:
[https://gitlab.com/failmap/server/blob/master/documentation/hosting.md](https://gitlab.com/failmap/server/blob/master/documentation/hosting.md)

### Operations

Operation instruction for the failmap server are documented here:
[https://gitlab.com/failmap/server/blob/master/documentation/operations.md](https://gitlab.com/failmap/server/blob/master/documentation/operations.md)


### Server architecture

Read more about the failmap server architecture here:
[https://gitlab.com/failmap/server](https://gitlab.com/failmap/server)
