��    2      �  C   <      H     I  Q   P     �     �     �     �     �     
           5     I     `     v     �     �     �     �     �          $     A     W  
   q  
   |  
   �  
   �  
   �     �     �     �     �     �     �  <     9   >  2   x  3   �     �     �  ,     +   ?  
   k  	   v     �     �  
   �     �     �     �  i  �     U
  u   b
     �
  '   �
  %        =  
   U     `     h     o     u     ~  
   �     �  
   �     �  	   �     �     �     �     �     �     �                    +     2     F     ]     l     �  &   �  '   �     �  %   �          4     Q     m     �     �     �     �     �     �     �     �  	   �     1      (   2      
               #           '   0                 &      %              .                    *   ,                    "   )                      	       !                    -   +         $      /                       DNSSEC Missing Strict-Transport-Security header. Offers no insecure alternative service. Search organization Strict-Transport-Security X-Frame-Options X-XSS-Protection category_menu_bundesland category_menu_country category_menu_county category_menu_cyber category_menu_district category_menu_finance category_menu_government category_menu_hacking category_menu_healthcare category_menu_municipality category_menu_province category_menu_regierungsbezirk category_menu_region category_menu_stadt_gemeinde category_menu_unknown category_menu_water_board country_AT country_DE country_GB country_NL country_SE ftp legend_basic_security not trusted plain_https report_header_DNSSEC report_header_ftp report_header_http_security_header_strict_transport_security report_header_http_security_header_x_content_type_options report_header_http_security_header_x_frame_options report_header_http_security_header_x_xss_protection report_header_plain_https report_header_tls_qualys report_header_tls_qualys_certificate_trusted report_header_tls_qualys_encryption_quality score high score low score medium score perfect tls_qualys tls_qualys_certificate_trusted tls_qualys_encryption_quality trusted Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-12-13 13:03+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 DNS Security Security Header not present: Strict-Transport-Security, yet this address offers no alternative insecure http service. Search organization... Strict-Transport-Security Header (HSTS) X-Frame-Options Header (clickjacking) X-XSS-Protection Header Bundesland Country County Cyber District Finance Government Hackers Healthcare Municipalities Provinces Regierungsberzirk Region Stadt / Gemeinde Unknown Water boards Austria Germany Great Brittain The Netherlands Sweden File transfer (FTP) The basic security is: niet vertrouwd Missing transport encryption DNSSEC Missing file transfer (FTP) encryption Strict-Transport-Security Header (HSTS) X-Content-Type-Options X-Frame-Options Header (clickjacking) X-XSS-Protection Header Missing transport encryption Encrypted transport (https) Trust in certificate Encryption quality high low medium perfect Encrypted transport (https) Trust in certificate Encryption quality vertrouwd 